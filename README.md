# Library Hackathon Challenge #

This Java project provides the Library interface as well as a simple example implementation by named after yours truly: TomsLibrary.

# Classes #

Library - The interface


TomsLibrary - Implementation by tom of library interface


TomsLibraryTest - JUnit tests for TomsLibrary that tests against the Library interface (test should ignores how anyone implemented it (just worries about if it behaves the way any Library must). Many more cases can be added!


Item - a item that can be sent to the library usually should be extended to provide more info on the item examples : book dvd cd periodic


ItemType - enum class used to represent the possible types of items, ideally this should be read from a config file and not defined in Java so you can add new Types without changing code.


LibraryItem - and item that is currently owned by the library, periodic library items are items the library has but cannot rent and must return to an owner at an expiry date.


ItemNotFoundException - An exception for when an item that was expected to be found was not thrown so the caller can decide how to react.
Should add more exceptions for specific cases such as trying to rent a periodic.
