package library;

import library.items.Item;
import library.items.ItemNotFoundException;
import library.items.LibraryItem;
import library.items.type.ItemType;
import library.items.type.Periodical;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

public class TomsLibrary implements Library {

    private final List<LibraryItem> items = new ArrayList<>();

    private static int idTracker;

    private static int generateId() {
        return idTracker++;
    }

    @Override
    public void addItem(Item item) {
        if (item.getType() != ItemType.PERIODICAL) {
            LibraryItem addedItem;
            items.add(addedItem = new LibraryItem(item, generateId()));
            System.out.println("Added new item " + addedItem.getName() + " with ID# " + addedItem.getLibraryCode());

        } else {
            // Wont add periodic without a expiry
            item.getExpiry().ifPresent(expiry -> {
                LibraryItem addedItem;
                items.add(addedItem = new Periodical(item, generateId(), item.getExpiry().get()));
                System.out.println("Added new periodical item " + addedItem.getName() + " with ID# " + addedItem.getLibraryCode());
            });
        }
    }

    @Override
    public void removeItem(int id) throws ItemNotFoundException {
        for (LibraryItem item : items) {
            if (item.getLibraryCode() == id) {
                items.remove(item);
                System.out.println("Removed item " + item.getName() + " with ID# " + item.getLibraryCode());
                return;
            }
        }
        throw new ItemNotFoundException("failed to delete item with id" + id);
    }

    @Override
    public List<LibraryItem> findItemsNamed(String name) {
        return items.stream()
                .filter(item -> item.getName().equals(name))
                .collect(toList());
    }

    @Override
    public Optional<LibraryItem> findItemWithId(int id) {
        return items.stream()
                .filter(item -> item.getLibraryCode() == id)
                .findAny();
    }

    @Override
    public void rentItem(int id) throws ItemNotFoundException {
        for (LibraryItem item : items) {
            if (item.getLibraryCode() == id) {
                if (item.isAvailable()) {
                    item.rent();
                    return;
                } else {
                    System.out.print("WARNING: library.items.Item#" + id + " was already rented!");
                }
            }
        }
        throw new ItemNotFoundException("failed to rent item with id" + id);
    }

    @Override
    public void returnItem(int id) throws ItemNotFoundException {
        for (LibraryItem item : items) {
            if (item.getLibraryCode() == id) {
                item.makeAvailable();
                return;
            }
        }
        throw new ItemNotFoundException("failed to return item with id" + id);
    }

    @Override
    public List<LibraryItem> getCatalogue() {
        // Returns a copy so no one modifies it from outside.
        return new ArrayList<>(items);
    }
}
