package library.items;

import library.items.type.ItemType;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

public class Item {

    private final String name;
    private final ItemType type;

    // Optionally an item may have an expected return date when library must give it back.
    private final Optional<Date> expiry;

    public Item(String name, ItemType type) {
        this.name = name;
        this.type = type;
        if (type != ItemType.PERIODICAL) {

            expiry = Optional.empty();
        }
        // If periodical and no expiry given set to half-year for follow up default.
        else {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, 180);
            expiry = Optional.of(c.getTime());
        }
    }

    public Item(String name, ItemType type, Date expiry) {
        this.name = name;
        this.type = type;
        this.expiry = Optional.of(expiry);
    }

    public String getName() {
        return name;
    }

    public ItemType getType() {
        return type;
    }

    public Optional<Date> getExpiry() {
        return expiry;
    }
}
