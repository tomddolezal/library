package library.items;

public class LibraryItem {

    private boolean availableToRent = true;
    private final Item item;
    private final int libraryCode;

    public LibraryItem(Item item, int code) {
        this.item = item;
        this.libraryCode = code;
    }

    public void rent() {
        this.availableToRent = false;
    }

    public void makeAvailable() {
        this.availableToRent = true;
    }

    public boolean isAvailable() {
        return availableToRent;
    }

    public Item getItem() {
        return item;
    }

    public String getName() {
        return this.item.getName();
    }

    public int getLibraryCode() {
        return libraryCode;
    }
}
