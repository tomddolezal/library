package library.items.type;

import library.items.Item;

import java.util.Date;

import static library.items.type.ItemType.BOOK;

public class Book extends Item {

    private String author;
    private Date published;

    public Book(String name, String author, Date published) {
        super(name, BOOK);
        this.author = author;
        this.published = published;
    }


}
