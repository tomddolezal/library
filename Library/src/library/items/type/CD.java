package library.items.type;

import library.items.Item;

import java.util.Date;

public class CD extends Item {

    private final Date published;
    private final String artist;
    private final String genre;

    public CD(String name, ItemType type, Date published, String artist, String genre) {
        super(name, ItemType.CD);
        this.published = published;
        this.artist = artist;
        this.genre = genre;
    }
}
