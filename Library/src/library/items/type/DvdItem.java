package library.items.type;

import library.items.Item;

import java.util.Date;

import static library.items.type.ItemType.DVD;

public class DvdItem extends Item {

    private double length;
    private String author;
    private Date published;

    public DvdItem(String name, double length, String author, Date published) {
        super(name, DVD);
        this.length = length;
        this.author = author;
        this.published = published;
    }
}
