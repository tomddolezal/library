package library.items.type;

import library.items.Item;
import library.items.LibraryItem;

import java.util.Date;

public class Periodical extends LibraryItem {

    // The date this periodical is leaving the library
    private final Date returnDate;
    private final Date acquired;

    public Periodical(Item item, int code, Date heldUntil) {
        super(item, code);
        returnDate = heldUntil;
        acquired = new Date();
    }

    @Override
    public boolean isAvailable() {
        return false;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public Date getAcquired() {
        return acquired;
    }
}
