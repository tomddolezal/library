package library.items.type;

public enum ItemType {
    BOOK, CD, DVD, PERIODICAL
}
