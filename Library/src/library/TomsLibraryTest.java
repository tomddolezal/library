package library;

import library.items.Item;
import library.items.ItemNotFoundException;
import library.items.LibraryItem;
import library.items.type.Book;
import library.items.type.ItemType;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class TomsLibraryTest {

    @Test
    public void shouldAddBasicBookItemToCatalogue() {
        Item book = new Book("Calvin and Hobbes", "auther of this", new Date());

        Library library = new TomsLibrary();
        library.addItem(book);
        List<LibraryItem> catalogue = library.getCatalogue();


        // Better to test with custom "isEqual" overrides instead of accessing name
        assertEquals("Calvin and Hobbes", catalogue.get(0).getName());
    }

    @Test
    public void shouldRemoveBookItemFromCatalogue() throws ItemNotFoundException {
        Item book = new Book("Calvin and Hobbes", "auther of this", new Date());

        Library library = new TomsLibrary();
        library.addItem(book);
        int id = library.getCatalogue().get(0).getLibraryCode();

        library.removeItem(id);


        // Better to test with custom "isEqual" overrides instead of accessing name
        assertEquals(library.getCatalogue().size(), 0);
    }

    @Test(expected = ItemNotFoundException.class)
    public void shouldThrowExceptionWhenRentPeriodicItem() throws ItemNotFoundException {
        Item book = new Item("Calvin and Hobbes", ItemType.PERIODICAL);

        Library library = new TomsLibrary();
        library.addItem(book);

        library.rentItem(library.getCatalogue().get(0).getLibraryCode());

    }

    @Test(expected = ItemNotFoundException.class)
    public void shouldThrowExceptionIfAttemptToRentATakenItem() throws ItemNotFoundException {
        Item book = new Item("Calvin and Hobbes", ItemType.DVD);

        Library library = new TomsLibrary();
        library.addItem(book);
        int code = library.getCatalogue().get(0).getLibraryCode();


        library.rentItem(code);
        library.rentItem(code);
    }

}