package library;

import library.items.Item;
import library.items.ItemNotFoundException;
import library.items.LibraryItem;

import java.util.List;
import java.util.Optional;

public interface Library {

    /**
     * Adds the {@Item} to the library catalogue.
     */
    public void addItem(Item item);

    /**
     * Adds the {@Item} to the library catalogue.
     */
    public void removeItem(int id) throws ItemNotFoundException;

    /**
     * Adds the {@Item} to the library catalogue. List is just empty for no results.
     *
     * @return list of all the items with the name
     */
    public List<LibraryItem> findItemsNamed(String name);

    /**
     * Returns a Optional in-case item no does not exist.
     */
    public Optional<LibraryItem> findItemWithId(int id) throws ItemNotFoundException;

    /**
     * Can only rent by ID since name can be same "Harry Potter 3" is in DVD/Book
     */
    public void rentItem(int id) throws ItemNotFoundException;

    /**
     * Returns a List containing all the library library.items that exist in this library.
     */
    public List<LibraryItem> getCatalogue();


    /**
     * @param id id for the item being returned.
     * @throws ItemNotFoundException No item with given id was found.
     */
    public void returnItem(int id) throws ItemNotFoundException;


}
